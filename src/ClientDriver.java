import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientDriver {

	private ZkConnector zkc = null;
	private Socket socket;
	
	
	public ClientDriver(String zkHost) throws Exception{
		zkc = new ZkConnector();
		try {
			zkc.connect(zkHost);
		} catch(Exception e) {
			System.out.println("Worker: Zookeeper connect Error "+ e);
		}
		if (zkc.exists(AppConstants.PRIMARY_JOB_TRACKER, null)==null){
			throw new Exception("Client couldn't find primary server");
		} else {
			String data = zkc.getData(AppConstants.PRIMARY_JOB_TRACKER, null, null);
			String[] jtInfo  = data.split(":");
			while (socket==null){
				try {
					socket = new Socket(jtInfo[0], Integer.parseInt(jtInfo[1]));
				} catch (Exception e) {
					System.out.println("CleintDriver: Error connecting to JT "+e);
				}
			}
		}
	}
	
	private void startJob (String hash) throws IOException, ClassNotFoundException{
		Socket jtSocket = socket;
		ObjectOutputStream outStream = new ObjectOutputStream(jtSocket.getOutputStream());
		outStream.flush();
		ObjectInputStream inStream = new ObjectInputStream(jtSocket.getInputStream());
		
		// send request packet to jobtracker 
		ClientRequestPacket reqPacket  = new ClientRequestPacket(hash, true, false);
		outStream.writeObject(reqPacket);
		outStream.flush();
		System.out.println("Cleint: Request Sent");
		//receive ack
		ClientRequestPacket respPacket;
		respPacket = (ClientRequestPacket) inStream.readObject();
		if (respPacket!=null && respPacket.getStatus()!=null){
			String status = respPacket.getStatus();
			System.out.println("Job Started, status: " + status);
		} else {
			System.out.println("CleintDriver:StartJob: Error receiving resp from jobTracker!");
		}
		jtSocket.close();
	}
	
	private String queryStatus (String hash) throws IOException, ClassNotFoundException{
		// send lookup request to zooKeeper
		// get addr of jobTracker and connect to it
		Socket jtSocket = socket;
		ObjectInputStream inStream = new ObjectInputStream(jtSocket.getInputStream());
		ObjectOutputStream outStream = new ObjectOutputStream(jtSocket.getOutputStream());
		outStream.flush();

		// send request packet to jobtracker 
		ClientRequestPacket reqPacket  = new ClientRequestPacket(hash, false, true);
		outStream.writeObject(reqPacket);
		outStream.flush();
		System.out.println("Cleint: Query Sent");
		//receive response
		ClientRequestPacket respPacket;
		respPacket = (ClientRequestPacket) inStream.readObject();
		String status = null;
		if (respPacket!=null && respPacket.getStatus()!=null){
			 status = respPacket.getStatus();
			System.out.println("Job Status: " + status);
		} else {
			System.out.println("CleintDriver:queryStatus: Error receiving resp from jobTracker!");
		}
		jtSocket.close();
		return status;
	}
	
	public static void main(String[] args){
		if (args.length != 3){
			System.out.println("Usage <zookeeper server>:<zookeeper port> job/status <password hash>");
			return;
		}
		String cmd = args[1];
		String hash = args[2];
		
		try {
			ClientDriver client = new ClientDriver(args[0]);
			
			if (AppConstants.JOB_CMD.equals(cmd)){
				client.startJob(hash);
				System.out.println("Job submitted");
			} else if (AppConstants.STATUS.equals(cmd)){
				String status = client.queryStatus(hash);
				System.out.println("Status: " + status);
			} else {
				System.out.println("Usage <zookeeper server>:<zookeeper port> job/status <password hash>");
				return;
			}
		} catch (Exception e) {
			System.out.println("Exception in ClientDriver: " + e);
			e.printStackTrace();
		}
		
	}
	
}
