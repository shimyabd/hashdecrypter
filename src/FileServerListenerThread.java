import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;


public class FileServerListenerThread implements Runnable{
	private Socket socket = null;
	private List<List<String>> partitionList = null;

	public FileServerListenerThread(Socket socket, List<List<String>> partitionList){
		this.socket = socket;
		this.partitionList = partitionList;
		if(AppConstants.DEBUG) System.out.println("Instantiating FileServerListenerThread");
	}

	public void run(){
		
		try {
			ObjectOutputStream outStream = new ObjectOutputStream(socket.getOutputStream());
			outStream.flush();
			ObjectInputStream inStream = new ObjectInputStream(socket.getInputStream());
			
			while(!socket.isClosed()){
				// receive request
				WorkerRequestPacket reqPakcet = (WorkerRequestPacket) inStream.readObject();
				// send requested partition back
				if (reqPakcet == null || reqPakcet.getId() < 0 || reqPakcet.getId() > partitionList.size()){
					System.out.println("FileServerListner: Error! Malformed request!");
				} else {
					int partId = reqPakcet.getId();
					if(AppConstants.DEBUG) System.out.println("FileServer: Request received for partition "+partId);
					List<String> partition = partitionList.get(partId);
					WorkerRequestPacket respPacket = new WorkerRequestPacket(partId, partition);
					outStream.writeObject(respPacket);
					outStream.flush();
					if(AppConstants.DEBUG) System.out.println("FileServer: Response sent");
				}
			}

		} catch (Exception e) {
			System.out.println("FileServerListener: Error: "+e);
			try {
				if(!socket.isClosed()) socket.close();
			} catch (IOException e1) {
			}
		}

	}

}
