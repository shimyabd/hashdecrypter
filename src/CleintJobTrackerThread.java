import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;

/*
 * Listens for new connections from clients
 */
public class CleintJobTrackerThread implements Runnable{

	private ServerSocket serverSocket = null;
	private ZkConnector zkc = null;
	private BlockingQueue<Task> taskQ = null;
	private Map<String, Task> workerTaskMap = null;

	public CleintJobTrackerThread(ServerSocket serverSocket, String zkHost, BlockingQueue<Task> taskQ, Map<String, Task> workerTaskMap){
		this.serverSocket=serverSocket;
		this.zkc = new ZkConnector();
		this.taskQ = taskQ;
		this.workerTaskMap = workerTaskMap;
		try {
			zkc.connect(zkHost);
		} catch(Exception e) {
			System.out.println("ClientJobTrackerThread: Zookeeper connect Error "+ e);
		}
		if(AppConstants.DEBUG) System.out.println("Instantiated ClientJTThread");
	}
	
	public List<Task> splitJob(String hash){
		int numPartitions = AppConstants.NUM_PARTITIONS;
		List<Task> list = new ArrayList<>();
		for(int i=0; i<numPartitions; i++){
			Task task = new Task(hash, i);
			if(AppConstants.DEBUG) System.out.println("CJTT: adding task to Q: "+task);
			if(taskQ.offer(task)==false) System.out.println("CleintJTThread Error: taskQ offer refused!");
			list.add(task);
		}
		return list;
	}

	public void run(){
		try {
			while(true){
				Socket clientSocket = serverSocket.accept();

				if(AppConstants.DEBUG) System.out.println("ClientJTThread: accepted new socket");

				ObjectOutputStream outStream = new ObjectOutputStream(clientSocket.getOutputStream());
				outStream.flush();
				ObjectInputStream inStream = new ObjectInputStream(clientSocket.getInputStream());



				ClientRequestPacket reqPacket = (ClientRequestPacket) inStream.readObject();
				if (reqPacket == null){
					System.out.println("Bad request packet!");
				} else {
					if(reqPacket.isStartJob()){
						if(AppConstants.DEBUG) System.out.println("Starting new Job");
						String hash =reqPacket.getHash();
						// make a job node
						zkc.create(AppConstants.JOBS+"/"+hash, AppConstants.IN_PROGRESS, CreateMode.PERSISTENT);
						// split into tasks
						splitJob(hash);
						for(int i=0 ; i<AppConstants.NUM_PARTITIONS; i++){
							// make partition/task nodes
							zkc.create(AppConstants.JOBS+"/"+hash+"/"+i, AppConstants.IN_PROGRESS, CreateMode.PERSISTENT);
						}
						// assign to workers
						List<String> workerList = null;
						workerList = zkc.getChildren(AppConstants.WORKERS, null, null);
						for (String worker : workerList){
							Stat stat = zkc.exists(AppConstants.WORKERS+"/"+worker, null);
							if (stat != null){
								synchronized (JobTracker.workersLock) {
									String workerStatus = zkc.getData(AppConstants.WORKERS+"/"+worker, null, null);
									if (workerStatus == null || workerStatus.isEmpty()){
										// free, assign job to this worker
										Task task = taskQ.poll();
										if (task!=null){
											if(AppConstants.DEBUG) System.out.println("CJTT: assigning task: "+task+" to: "+worker);
											workerTaskMap.put(worker, task);
											// mark as busy
											zkc.setData(AppConstants.WORKERS+"/"+worker, null, task.toString());
											//TODO put a watch on the state ?
										}
									}
								}
							}
						}
						
						System.out.println("CJTT: started Job for: "+reqPacket.getHash());
						// return response
						ClientRequestPacket respPacket = new ClientRequestPacket(AppConstants.IN_PROGRESS);
						outStream.writeObject(respPacket);
					} else if(reqPacket.isStatusQuery()){
						//do the query
						if(AppConstants.DEBUG) System.out.println("Getting status for: "+reqPacket.getHash());
						String status = getStatus(reqPacket.getHash());
						// return response
						if(AppConstants.DEBUG) System.out.println("CJTT returning status: "+status);
						ClientRequestPacket respPacket = new ClientRequestPacket(status);
						outStream.writeObject(respPacket);
					} else {
						System.out.println("Unkown request type.");
					}

				}
				clientSocket.close();
			}
		} catch (Exception e) {
			try {
				serverSocket.close();
			} catch (IOException e1) {
			}
			System.out.println("Exception in ClientJT: "+e);
			e.printStackTrace();
		}
	}
	
	private String getStatus(String hash){
		if (zkc.exists(AppConstants.JOBS+"/"+hash, null)==null){
			return AppConstants.NOT_FOUND;
		}
		String jobStatus = zkc.getData(AppConstants.JOBS+"/"+hash , null, null);
		if (jobStatus==null || jobStatus.isEmpty()){
			System.out.println("JT getstatus: job status is null for "+hash);
			return AppConstants.FAILED;
		}
		else if (jobStatus.contains(AppConstants.SUCCESS)){
			return jobStatus;
		} else if(jobStatus.equals(AppConstants.COMPLETE)){
			return jobStatus;
		} // else is in progress but may actually be completed. assume completed until we know
		jobStatus = AppConstants.COMPLETE;
		List<String> parts = zkc.getChildren(AppConstants.JOBS+"/"+hash , null, null);
		for (String part:parts){
			String partStatus = zkc.getData(AppConstants.JOBS+"/"+hash+"/"+part, null, null);
			if (partStatus!=null){
				if (partStatus.contains(AppConstants.SUCCESS)){
					return partStatus;
				} else if (partStatus.equals(AppConstants.COMPLETE)){
				} else {
					jobStatus = AppConstants.IN_PROGRESS;
					// another one could have success? but we sure can't be completed
				}
			} else {
				// shouldn't be here
				System.out.println("JT getstatus: task status is null for "+hash+","+part);
				jobStatus = AppConstants.FAILED;
			}
		}
		return jobStatus;
	}
}
