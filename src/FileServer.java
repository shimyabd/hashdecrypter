import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;

public class FileServer {
	
	private ZkConnector zkc = null;
	private ServerSocket serverSocket = null;
	private List<List<String>> partitionList = null;
	
	
	public FileServer(String zkHost) throws IOException{
		serverSocket = new ServerSocket(0);
		partitionList = new ArrayList<>();
		zkc = new ZkConnector();
		try {
			zkc.connect(zkHost);
			initZNodes();
		} catch(Exception e) {
			System.out.println("JobTracker: Zookeeper connect Error "+ e);
		}
	}
	
	void initZNodes(){
		if (zkc.exists(AppConstants.JOBS, null) == null){
			Code ret = zkc.create(AppConstants.JOBS, null,CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created jobs zNode");
		}
		if (zkc.exists(AppConstants.WORKERS, null) == null){
			Code ret = zkc.create(AppConstants.WORKERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created workers zNode");
		}
		if (zkc.exists(AppConstants.JOB_TRACKERS, null) == null){
			Code ret = zkc.create(AppConstants.JOB_TRACKERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created jTrackers zNode");
		}
		if (zkc.exists(AppConstants.FILE_SERVERS, null) == null){
			Code ret = zkc.create(AppConstants.FILE_SERVERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created fileServers zNode");
		}
	}
	
	/*
	 * Clears /jobs and all children
	 */
	private void clear() throws KeeperException, InterruptedException{
		List<String> children = zkc.getZooKeeper().getChildren(AppConstants.JOBS, null);
    	
    	for(String child : children) {
    		List<String> children2 = zkc.getZooKeeper().getChildren(AppConstants.JOBS+ "/" + child, null);
    		for(String child2 : children2) {
        		zkc.getZooKeeper().delete(AppConstants.JOBS + "/" + child + "/" + child2, -1);
        	}
    		zkc.getZooKeeper().delete(AppConstants.JOBS + "/" + child, -1);
    	}
    	zkc.getZooKeeper().delete(AppConstants.JOBS, -1);
	}
	
    public void handleEvent(WatchedEvent event) {
        String path = event.getPath();
        EventType type = event.getType();
        if(path.equalsIgnoreCase(AppConstants.PRIMARY_FILE_SERVERS)) {
            if (type == EventType.NodeDeleted) {
                System.out.println(AppConstants.PRIMARY_FILE_SERVERS + " deleted! Let's go!");  
                checkpath();
            }
        }
    }
	
	private void checkpath() {
		try {
			Watcher watcher = new Watcher() {
				public void process(WatchedEvent event) {
					handleEvent(event);
				}
			};
			String hostname = InetAddress.getLocalHost().getHostName();
			int port = serverSocket.getLocalPort();
			String data = hostname + ":" + port;
			if (zkc.exists(AppConstants.PRIMARY_FILE_SERVERS, watcher) == null) {
				System.out.println("FS: Creating " + AppConstants.PRIMARY_FILE_SERVERS);         
				Code ret = zkc.create(AppConstants.PRIMARY_FILE_SERVERS, data, CreateMode.EPHEMERAL);
				if (ret == Code.OK) System.out.println("FS: Created primary FS Node");
			} else {
				System.out.println("FS: Creating " + AppConstants.BACKUP_FILE_SERVERS);
				Code ret = zkc.create(AppConstants.BACKUP_FILE_SERVERS, data, CreateMode.EPHEMERAL);
				if (ret == Code.OK) System.out.println("FS: Created backup FS Node");
			}

		} catch (Exception e) {
			System.out.println("FS: error in checkpath: "+e);
			e.printStackTrace();
		}

	}
	
	/*
	 * Loads file into partitionList
	 * returns true on success
	 */
	private boolean loadFilePartitions(String fileName) {
		try {
			File file = new File(fileName);
			if (!file.exists() || !file.isFile()){
				System.out.println("FS: Error: Invalid file");
				return false;
			} else {
//				FileReader fileReader = new FileReader(file);
//				BufferedReader buffReader = new BufferedReader(fileReader);
				List<String> lines = Files.readAllLines(Paths.get(file.toURI()), Charset.forName(AppConstants.ENCODING));
				// each line is a word, split into partitions
				int numOfPartitions = (lines.size()+AppConstants.PARTITION_SIZE-1)/AppConstants.PARTITION_SIZE;
				AppConstants.NUM_PARTITIONS = numOfPartitions;
				for (int i=0; i<numOfPartitions; i++){
					List<String> partition = new ArrayList<>();
					int from = i*AppConstants.PARTITION_SIZE;
					int to = from + AppConstants.PARTITION_SIZE;
					if (to > lines.size()) to = lines.size();
					
					partition.addAll(lines.subList(from, to));
					partitionList.add(partition);
				}
				return true;
			}
		} catch (Exception e) {
			System.out.println("Error Loading File: "+e);
			e.printStackTrace();
			return false;
		}
	}
	
	private void listenForConnections() throws IOException{
		while(!serverSocket.isClosed()){
			if (AppConstants.DEBUG) System.out.println("FS: Listening for connections...");
			Socket socket = serverSocket.accept();
			// start dedicated listener thread
			new Thread(new FileServerListenerThread(socket, partitionList)).start();
		}
	}
	
	
	public static void main(String[] args){
		if (args.length != 2){
			System.out.println("Usage FileServer <zookeeper serer>:<zookeeper port> <dictionary file>");
			return;
		}
		String fileName = args[1];
		
		try {
			FileServer fileServer = new FileServer(args[0]);
//			fileServer.clear();
			// primary/backup
			fileServer.checkpath();
			boolean succ = fileServer.loadFilePartitions(fileName);
			if (succ){
				if (AppConstants.DEBUG) System.out.println("FS: File Loaded");
				// listen for worker connections and requests
				fileServer.listenForConnections();
			}
		} catch (Exception e) {
			System.out.println("FileServer Error: "+e);
			e.printStackTrace();
		}
		
	}
}
