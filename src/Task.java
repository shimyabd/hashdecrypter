

public class Task {
	protected String hash = null;
	protected int partitionId;
	
	public Task(String hash, int partId){
		this.hash = hash;
		this.partitionId = partId;
	}
	
	public Task (String taskStr){
		String[] split = taskStr.split(",");
		this.hash = split[0];
		this.partitionId = Integer.parseInt(split[1]);
	}
	
	public String toString(){
		return hash+","+partitionId;
	}
}
