import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.data.Stat;


public class JobTracker {
	
	public static Object workersLock = new Object();
	
	private ServerSocket serverSocket = null;
	private ZkConnector zkc = null;
	private BlockingQueue<Task> taskQ = null;
	private Map<String, Task> workerTaskMap = null;
	private List<String> workerList = null;
	
	public JobTracker(String zkHost) throws IOException{
		serverSocket = new ServerSocket(0);
		taskQ = new LinkedBlockingQueue<>();
		workerTaskMap = new ConcurrentHashMap<String, Task>();
		workerList = new ArrayList<String>();
		zkc = new ZkConnector();
		try {
			zkc.connect(zkHost);
			initZNodes();
		} catch(Exception e) {
			System.out.println("JobTracker: Zookeeper connect Error "+ e);
		}
	}
	
	void initZNodes(){
		if (zkc.exists(AppConstants.JOBS, null) == null){
			Code ret = zkc.create(AppConstants.JOBS, null,CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created jobs zNode");
		}
		if (zkc.exists(AppConstants.WORKERS, null) == null){
			Code ret = zkc.create(AppConstants.WORKERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created workers zNode");
		}
		if (zkc.exists(AppConstants.JOB_TRACKERS, null) == null){
			Code ret = zkc.create(AppConstants.JOB_TRACKERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created jTrackers zNode");
		}
		if (zkc.exists(AppConstants.FILE_SERVERS, null) == null){
			Code ret = zkc.create(AppConstants.FILE_SERVERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created fileServers zNode");
		}
	}
	
    public void handleEvent(WatchedEvent event) {
        String path = event.getPath();
        EventType type = event.getType();
        if(path.equalsIgnoreCase(AppConstants.PRIMARY_JOB_TRACKER)) {
            if (type == EventType.NodeDeleted) {
                System.out.println(AppConstants.PRIMARY_JOB_TRACKER + " deleted! Let's go!");  
                checkpath();
            }
        }
    }
	
	private void checkpath() {
		try {
			Watcher watcher = new Watcher() {
				public void process(WatchedEvent event) {
					handleEvent(event);
				}
			};
			String hostname = InetAddress.getLocalHost().getHostName();
			int port = serverSocket.getLocalPort();
			String data = hostname + ":" + port;
			if (zkc.exists(AppConstants.PRIMARY_JOB_TRACKER, watcher) == null) {
				System.out.println("JT: Creating " + AppConstants.PRIMARY_JOB_TRACKER);         
				Code ret = zkc.create(AppConstants.PRIMARY_JOB_TRACKER, data, CreateMode.EPHEMERAL);
				if (ret == Code.OK) System.out.println("JT: Created primary FS Node");
			} else {
				System.out.println("JT: Creating " + AppConstants.BACKUP_JOB_TRACKER);
				Code ret = zkc.create(AppConstants.BACKUP_JOB_TRACKER, data, CreateMode.EPHEMERAL);
				if (ret == Code.OK) System.out.println("JT: Created backup FS Node");
			}

		} catch (Exception e) {
			System.out.println("JT: error in checkpath: "+e);
			e.printStackTrace();
		}

	}
	
	/*
	 * sets up workertaskMap and taskQ from zk
	 */
	private void setupJobs(){
		List<String> jobList = zkc.getChildren(AppConstants.JOBS, null, null);
		if (jobList!=null && !jobList.isEmpty()){
			for (String job:jobList){
				String jobStatus = zkc.getData(AppConstants.JOBS+"/"+job, null, null);
				if (jobStatus!=null && 
					(jobStatus.contains(AppConstants.SUCCESS)||jobStatus.equals(AppConstants.COMPLETE))){
					// job done
				} else{
					List<String> tasks = zkc.getChildren(AppConstants.JOBS+"/"+job, null, null);
					//TODO corner case: no tasks under job?
					for(String partId :tasks){
						String taskStatus = zkc.getData(AppConstants.JOBS+"/"+job+"/"+partId, null, null);
						if (taskStatus!=null && 
							(taskStatus.contains(AppConstants.SUCCESS)||taskStatus.equals(AppConstants.COMPLETE))){
							//done
						} else{
							Task taskObj = new Task(job,Integer.parseInt(partId));
							if (!checkTaskInProgress(taskObj)){
								if (!taskQ.offer(taskObj)) System.out.println("JT setup Jobs Error job refused");
							}
						}
					}
				}
			}
		}
	}
	
	/*
	 * checks if task is being worked on, if so puts it on worker-task map
	 */
	private boolean checkTaskInProgress(Task task){
		List<String> workers = zkc.getChildren(AppConstants.WORKERS, null, null);
		if (workers!=null){
			for (String worker: workers){
				String workerTask = zkc.getData(AppConstants.WORKERS+"/"+worker, null, null);
				if (workerTask!=null && workerTask.equals(task.toString())){
					if (AppConstants.DEBUG) System.out.println("JT setup found worker on task: "+task);
					workerTaskMap.put(worker, task);
					return true;
				}
			}
		}
		return false;
	}
	
	/* 
	 * get workerIds and set watcher on new workers
	 */
	private void getWorkers(){
		try {
			Stat stat = new Stat();
			List<String> children = zkc.getChildren("/workers", workerChangeWatcher, stat);
			if (children != null){
				reassignTasksAndSetWorkerList(children);
			} else {
				System.out.println("JT: Couldn't get workers from zk");
				getWorkers(); //TODO is this correct?
			}
		} catch (InterruptedException e) {
			System.out.println("JT error getting and setting workers: "+e);
			e.printStackTrace();
		}
	}

	void reassignTasksAndSetWorkerList(List<String> children) throws InterruptedException {
		if(workerList == null) {
			if (AppConstants.DEBUG) System.out.println("JT: Initilising worker list cache");
			workerList = new ArrayList<>(children);
		} else {
			if (!children.containsAll(workerList)){
				// worker(s) died, reassign tasks
				if (AppConstants.DEBUG) System.out.println("JT: Workers died, reassigning tasks");
				List<String> lostWorkers = new ArrayList<>(workerList);
				lostWorkers.removeAll(children);
				// remove them from our worker pool cache
				workerList.removeAll(lostWorkers);
				// reassign tasks
				for (String worker: lostWorkers){
					Task task = workerTaskMap.get(worker);
					if(AppConstants.DEBUG) System.out.println("JT: Worker died: "+worker);
					if(task!=null){
						if(AppConstants.DEBUG) System.out.println("JT: putting on q: "+task);
						if(taskQ.offer(task)==false) System.out.println("JT Error: taskQ offer refused!");
						workerTaskMap.remove(worker);
					}
				}
			}
			// could also have new workers
			List<String> newWorkers = new ArrayList<>(children);
			newWorkers.removeAll(workerList);
			if (AppConstants.DEBUG && !newWorkers.isEmpty()) System.out.println("JT: Adding new workers");
			// add them to our workerpool cache
			workerList.addAll(newWorkers);
			
		}
		assignTasks();
	}
	
	Watcher workerChangeWatcher = new Watcher() {
		public void process(WatchedEvent e) {
			if(e.getType() == EventType.NodeChildrenChanged) {
	            assert "/workers".equals( e.getPath() );
	            if(AppConstants.DEBUG) System.out.println("JT: Got a new worker event");
	            getWorkers();
	        }
		}
	};
	
	private void assignTasks(){
		for (String worker:workerList){
			checkWorker(worker);
		}
	}
	private void checkWorker(String worker) {
		Watcher watcher = new Watcher() {
			public void process(WatchedEvent event) {
				handleWorkerState(event);
			}
		};
		Stat stat = zkc.exists(AppConstants.WORKERS+"/"+worker, watcher);
		if (stat!=null){
			synchronized (workersLock) {
				String workerTask = zkc.getData(AppConstants.WORKERS+"/"+worker, null, null);
				if (workerTask==null || workerTask.isEmpty()){
					assignTaskToWorker(AppConstants.WORKERS+"/"+worker);
				}
			}
		}
	}

	private void handleWorkerState(WatchedEvent event){
		String path = event.getPath();
		EventType type = event.getType();
		if (type == EventType.NodeDataChanged){
			// worker finished processing, update jobStatus and taskMap
			String worker = path.split("/")[2];
			Task oldTask = workerTaskMap.get(worker);
			if (oldTask!=null){
				try {
					String taskStat = zkc.getData(AppConstants.JOBS+"/"+oldTask.hash+"/"+oldTask.partitionId, null, null);
					if (taskStat!=null){
						if (taskStat.equals(AppConstants.COMPLETE)){
							String jobStatus = getStatus(oldTask.hash);
							if(jobStatus.equals(AppConstants.COMPLETE)){
								zkc.setData(AppConstants.JOBS+"/"+oldTask.hash , null, AppConstants.COMPLETE);
							}
						}
						if (taskStat.equals(AppConstants.COMPLETE) || taskStat.contains(AppConstants.SUCCESS)){
							workerTaskMap.remove(worker);
						}
					}
				} catch (Exception e) {
					System.out.println("JT Error getting task status "+e);
					e.printStackTrace();
				}
			}
			checkWorker(worker);
		}
		// else do nothing, maybe debug statement
	}

	private void assignTaskToWorker(String workerPath) {
		// assign new task, update map
		Task task = taskQ.poll();
		if (task!=null){
			if(AppConstants.DEBUG) System.out.println("JT: assigning task: "+task+" to: "+workerPath);
			// CANNOT create child for ephem node
			zkc.setData(workerPath, null, task.toString());
			String worker = workerPath.replaceFirst("/workers/", "");
			workerTaskMap.put(worker, task);
		}
	}
	
	private void startThread(String zkHost){
		new Thread(new CleintJobTrackerThread(serverSocket, zkHost, taskQ, workerTaskMap)).start();
	}
	
	
	private String getStatus(String hash){
		if (zkc.exists(AppConstants.JOBS+"/"+hash, null)==null){
			return AppConstants.NOT_FOUND;
		}
		String jobStatus = zkc.getData(AppConstants.JOBS+"/"+hash , null, null);
		if (jobStatus==null || jobStatus.isEmpty()){
			System.out.println("JT getstatus: job status is null for "+hash);
			return AppConstants.FAILED;
		}
		else if (jobStatus.contains(AppConstants.SUCCESS)){
			return jobStatus;
		} else if(jobStatus.equals(AppConstants.COMPLETE)){
			return jobStatus;
		} // else is in progress but may actually be completed. assume completed until we know
		jobStatus = AppConstants.COMPLETE;
		List<String> parts = zkc.getChildren(AppConstants.JOBS+"/"+hash , null, null);
		for (String part:parts){
			String partStatus = zkc.getData(AppConstants.JOBS+"/"+hash+"/"+part, null, null);
			if (partStatus!=null){
				if (partStatus.contains(AppConstants.SUCCESS)){
					return partStatus;
				} else if (partStatus.equals(AppConstants.COMPLETE)){
				} else {
					jobStatus = AppConstants.IN_PROGRESS;
					// another one could have success? but we sure can't be completed
				}
			} else {
				// shouldn't be here
				System.out.println("JT getstatus: task status is null for "+hash+","+part);
				jobStatus = AppConstants.FAILED;
			}
		}
		return jobStatus;
	}
	
	public static void main(String[] args){
		if (args.length != 1){
			System.out.println("Usage <zookeeper server>:<zookeeper port>");
			return;
		}
		try {
			JobTracker jobTracker = new JobTracker(args[0]);
			jobTracker.checkpath();
			jobTracker.setupJobs();
			jobTracker.startThread(args[0]);
			jobTracker.getWorkers();
			jobTracker.assignTasks();
			while(true);
			
		} catch (Exception e) {
			System.out.println("Exception in ClientDriver: " + e);
			e.printStackTrace();
		}
	}
}
