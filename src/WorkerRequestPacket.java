import java.io.Serializable;
import java.util.List;


public class WorkerRequestPacket implements Serializable {
	private static final long serialVersionUID = 4815579155454122078L;
	
	private int id;
	private List<String> partition = null;
	
	
	public WorkerRequestPacket(int partitionId){
		this.id = partitionId;
	}
	
	public WorkerRequestPacket(int partitionId, List<String> partition){
		this.id = partitionId;
		this.partition = partition;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<String> getPartition() {
		return partition;
	}

	public void setPartition(List<String> partition) {
		this.partition = partition;
	}

}
