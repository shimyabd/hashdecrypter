
public class AppConstants {
	
	public static final boolean DEBUG = true;
	
	// dictionary encoding
	public static final String ENCODING = "US-ASCII"; //ISO-8859-1 UTF-8
	public static final int PARTITION_SIZE = 100000;
	public static int NUM_PARTITIONS = 3;
	
	// CleintDriver cmds
	public static final String JOB_CMD = "job";
	public static final String STATUS = "status";
	
	// Statuses
	public static final String IN_PROGRESS = "In Progress";
	public static final String COMPLETE = "Failed: Password not found";
	public static final String SUCCESS = "Password found";
	public static final String FAILED = "Failed: Failed to complete job";
	public static final String NOT_FOUND = "Failed: Job not found";
	
	// Worker status
	public static final String FREE_WORKER = "free";
	public static final String BUSY_WORKER = "busy";
	
	// root nodes
	public static final String JOBS = "/jobs";
	public static final String WORKERS = "/workers";
	public static final String JOB_TRACKERS = "/jobTrackers";
	public static final String FILE_SERVERS = "/fileServers";
	// primary and backup nodes
	public static final String PRIMARY_JOB_TRACKER = "/jobTrackers/primary";
	public static final String BACKUP_JOB_TRACKER = "/jobTrackers/backup";
	public static final String PRIMARY_FILE_SERVERS = "/fileServers/primary";
	public static final String BACKUP_FILE_SERVERS = "/fileServers/backup";

	
	// JobTracker port
	public static final int JT_PORT = 8050;
	public static final int FSPORT = 8020;

}
