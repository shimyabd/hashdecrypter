import java.io.Serializable;


public class ClientRequestPacket implements Serializable {
	private static final long serialVersionUID = 1519721231338770133L;
	
	private String hash = null;
	private boolean startJob;
	private boolean statusQuery;
	private String status = null;
	
	
	public ClientRequestPacket(String status){
		this.status = status;
	}
	
	public ClientRequestPacket(String hash, boolean startJob, boolean statusQuery){
		this.hash = hash;
		this.startJob = startJob;
		this.statusQuery = statusQuery;
	}


	public String getHash() {
		return hash;
	}


	public void setHash(String hash) {
		this.hash = hash;
	}


	public boolean isStartJob() {
		return startJob;
	}


	public void setStartJob(boolean startJob) {
		this.startJob = startJob;
	}


	public boolean isStatusQuery() {
		return statusQuery;
	}


	public void setStatusQuery(boolean statusQuery) {
		this.statusQuery = statusQuery;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
}
