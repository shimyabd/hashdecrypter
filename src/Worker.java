import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;


public class Worker {

	private Socket socket;
	private ObjectOutputStream outStream = null;
	private ObjectInputStream inStream = null;
	private String hashToBreak = null;
	private int partitionId;
	// the word list I'm searching through
	private List<String> wordList = null;
	private ZkConnector zkc = null;
	private String myId = "theWorker";
	private String myPath = null;
	
	private Watcher watcher = new Watcher() {
		    public void process(WatchedEvent event) {
		        handleEvent(event);
		    }
     };
	
	public Worker(String zkHost) throws Exception{
		zkc = new ZkConnector();
		try {
			zkc.connect(zkHost);
			initZNodes();
		} catch(Exception e) {
			System.out.println("Worker: Zookeeper connect Error "+ e);
		}
		if (zkc.exists(AppConstants.PRIMARY_FILE_SERVERS, watcher)==null){
			throw new Exception("Worker couldn't find primary file server");
		} else {
			String data = zkc.getData(AppConstants.PRIMARY_FILE_SERVERS, null, null);
			String[] fsInfo  = data.split(":");
			socket = new Socket(fsInfo[0], Integer.parseInt(fsInfo[1]));
			outStream = new ObjectOutputStream(socket.getOutputStream());
			outStream.flush();
			inStream = new ObjectInputStream(socket.getInputStream());
		}
		
	}
	
	void initZNodes() throws KeeperException, InterruptedException{
		if (zkc.exists(AppConstants.JOBS, null) == null){
			Code ret = zkc.create(AppConstants.JOBS, null,CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created jobs zNode");
		}
		if (zkc.exists(AppConstants.WORKERS, null) == null){
			Code ret = zkc.create(AppConstants.WORKERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created workers zNode");
		}
		if (zkc.exists(AppConstants.JOB_TRACKERS, null) == null){
			Code ret = zkc.create(AppConstants.JOB_TRACKERS, null, CreateMode.PERSISTENT);
			if (ret == Code.OK) System.out.println("Created jTrackers zNode");
		}
		// create node for myself
		myPath = zkc.getZooKeeper().create(AppConstants.WORKERS + "/worker", null, 
				ZkConnector.acl, CreateMode.EPHEMERAL_SEQUENTIAL);
		myId = myPath.split("/")[2];
		myPath = AppConstants.WORKERS+"/"+myId;
		System.out.println("Worker started: "+myId);
	}
	
	private void handleEvent(WatchedEvent event){
		String path = event.getPath();
        EventType type = event.getType();
        
        if(path.equals(AppConstants.PRIMARY_FILE_SERVERS)) {
            if (type == EventType.NodeDeleted) {
            	System.out.println("Worker switching to backup fileServer");
            	String fs = null;
            	if (zkc.exists(AppConstants.PRIMARY_FILE_SERVERS, watcher) != null){
            		fs = AppConstants.PRIMARY_FILE_SERVERS;
            	} else if(zkc.exists(AppConstants.BACKUP_FILE_SERVERS, null) != null) {
            		fs = AppConstants.BACKUP_FILE_SERVERS;
                } else {
                	System.out.println("Can't find any file servers");
                	System.exit(-1);
                }
                
                try {
                	String data = zkc.getData(fs, null, null);
        			String[] fsInfo  = data.split(":");
        			socket = new Socket(fsInfo[0], Integer.parseInt(fsInfo[1]));
        			outStream = new ObjectOutputStream(socket.getOutputStream());
        			outStream.flush();
        			inStream = new ObjectInputStream(socket.getInputStream());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
                
            }
        }
	}
	
	/*
	 * return true on success, else false
	 */
	private boolean setUpWordList(){
		try {
			if (socket.isConnected() && !socket.isClosed()){
				WorkerRequestPacket reqPacket = new WorkerRequestPacket(partitionId);
				outStream.writeObject(reqPacket);
				outStream.flush();
				if(AppConstants.DEBUG) System.out.println("Worker: Request sent for partition "+partitionId);
				
				WorkerRequestPacket respPakcet = (WorkerRequestPacket) inStream.readObject();
				if (respPakcet!=null && respPakcet.getPartition()!=null && !respPakcet.getPartition().isEmpty()
						&& respPakcet.getId() == partitionId){
					if(AppConstants.DEBUG) System.out.println("Worker: Response received successfuly");
					wordList = new ArrayList<>();
					wordList.addAll(respPakcet.getPartition());
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println("Worker: Error Connecting to fileServer: "+e);
			e.printStackTrace();
		}
		return false;
	}

	
	// TODO make watcher and make it call this instead of polling
	private Task getTask(){
		String taskStr = zkc.getData(myPath, null, null); 
		if (taskStr!=null && !taskStr.isEmpty()){
			if (AppConstants.DEBUG)	System.out.println("Worker found assigned task: "+taskStr);
			Task task  = new Task(taskStr);
			this.hashToBreak = task.hash;
			this.partitionId = task.partitionId;
			return task;
		}
		return null;
	}
	
	/*
	 * returns plaintext password if found, null otherwise
	 */
	private String breakHash(){
		for (String word: wordList){
			String hash = getHash(word);
			if (hash != null){
				if(hash.equals(hashToBreak)){
					// FOUND
					return word;
				}
			}
		}
		return null;
	}
	
	private String getHash(String word) {
	    String hash = null;
	    try {
	        MessageDigest md5 = MessageDigest.getInstance("MD5");
	        BigInteger hashint = new BigInteger(1, md5.digest(word.getBytes()));
	        hash = hashint.toString(16);
	        while (hash.length() < 32) hash = "0" + hash;
	    } catch (NoSuchAlgorithmException nsae) {
	    	System.out.println("Worker: Error: "+nsae);
	        // ignore
	    }
	    return hash;
	}
	
	private void doWork(){
		boolean succ = setUpWordList();
		String password = null;
		if (succ){
			password = breakHash();
			String data = null;
			if (password == null ){
				System.out.println("Search completed, password not found");
				data = AppConstants.COMPLETE;
			} else {
				System.out.println("Password Found !! It is "+password);
				data = AppConstants.SUCCESS + ": " + password;
				zkc.setData(AppConstants.JOBS+"/"+hashToBreak, null, data);
			}
			zkc.setData(AppConstants.JOBS+"/"+hashToBreak+"/"+partitionId, null, data);
			// say I'm free?
			zkc.setData(myPath, null, "");
		} else {
			System.out.println("Failed to load partition from fileServer");
		}
	}
	
	public static void main(String[] args){
		if (args.length != 1){
			System.out.println("Usage <zookeeper server>:<zookeeper port>");
			return;
		}
		try {
			Worker worker = new Worker(args[0]); // c463be62fd5252c7568d7bafd3cc4a55 == r41nb0w
			while (true){
				Task task = null;
				while (task == null){
					task = worker.getTask();
				}
				worker.doWork();
			}
		} catch (Exception e) {
			System.out.println("Exception in Worker: " + e);
			e.printStackTrace();
		}
	}
	
}

